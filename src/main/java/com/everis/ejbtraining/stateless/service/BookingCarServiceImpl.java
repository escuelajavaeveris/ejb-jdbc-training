package com.everis.ejbtraining.stateless.service;

import com.everis.ejbtraining.stateless.interceptor.SecurityInterceptor;
import com.everis.ejbtraining.stateless.manager.BookingCarManager;
import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;

/**
 * Created by hgranjal on 26/06/2018.
 */
@Stateless
//@TransactionManagement(value = TransactionManagementType.BEAN)
public class BookingCarServiceImpl implements BookingCarService {

    @Inject
    private BookingCarManager bookingCarManager;
    @EJB
    private ProviderCarService providerCarService;
    @Resource(name = "bookings-ds")
    private DataSource dataSource;

    public BookingCarServiceImpl() {
    }

    @Interceptors(SecurityInterceptor.class)
    @Override
    //@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Car> findCarAll(String username) {
        List<Car> carList = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            carList = bookingCarManager.findCarAll(connection);
        } catch (SQLException e) {
            System.out.println("Error getting SQL connection: " + e.getMessage());
        }

        return carList;
    }

    @Interceptors(SecurityInterceptor.class)
    @Override
    //@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<BookingCar> findBookingCarAll(String username) {
        List<BookingCar> bookingCarList = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            bookingCarList = bookingCarManager.findBookingCarAll(connection);
        } catch (SQLException e) {
            System.out.println("Error getting SQL connection: " + e.getMessage());
        }

        return bookingCarList;
    }

    //TODO: set key of bookingCar table only to idcar? (not correct but efficient!)
    //TODO: implement transaction safety (@TransactionAttribute)
    @Interceptors(SecurityInterceptor.class)
    @Override
    //@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public BookingCar book(String username, Car car, String user) {
        BookingCar bookingCar = null;

        try (Connection connection = dataSource.getConnection()) {
            //connection.setAutoCommit(false);

            bookingCar = bookingCarManager.book(car, user, connection);
            if (bookingCar == null) {
                Car newCar = providerCarService.buyCar(car);
                bookingCar = bookingCarManager.book(newCar, user, connection);
            }

            //connection.commit();
        } catch (SQLException e) {
            System.out.println("Error getting SQL connection: " + e.getMessage());
        }

        return bookingCar;
    }

    @Interceptors(SecurityInterceptor.class)
    @Override
    //@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void remove(String username, BookingCar bookingCar) {

        try (Connection connection = dataSource.getConnection()) {
            bookingCarManager.remove(bookingCar, connection);
        } catch (SQLException e) {
            System.out.println("Error getting SQL connection: " + e.getMessage());
        }
    }
}
