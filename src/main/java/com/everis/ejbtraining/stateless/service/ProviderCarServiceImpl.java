package com.everis.ejbtraining.stateless.service;

import com.everis.ejbtraining.stateless.manager.ProviderCarManager;
import com.everis.ejbtraining.stateless.model.Car;
import com.everis.ejbtraining.stateless.store.ConnectionDummy;

import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.inject.Inject;
import javax.sql.DataSource;

/**
 * Created by hgranjal on 26/06/2018.
 */
@Stateless
//@TransactionManagement(value = TransactionManagementType.BEAN)
public class ProviderCarServiceImpl implements ProviderCarService {

    @Inject
    private ProviderCarManager providerCarManager;
    @Resource(name = "bookings-ds")
    private DataSource dataSource;

    public ProviderCarServiceImpl() {
    }

    @Override
    //@TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Car buyCar(Car car) {
        Car newCar = null;

        try (Connection connection = dataSource.getConnection()) {
            newCar = providerCarManager.buyCar(car, connection);
        } catch (SQLException e) {
            System.out.println("Error getting SQL connection: " + e.getMessage());
        }

        return newCar;
    }
}
