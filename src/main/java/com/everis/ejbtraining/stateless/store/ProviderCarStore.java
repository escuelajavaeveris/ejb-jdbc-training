package com.everis.ejbtraining.stateless.store;

import com.everis.ejbtraining.stateless.model.Car;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by hgranjal on 26/06/2018.
 */
@ApplicationScoped
public class ProviderCarStore {

    private static final String INSERT_CAR = "INSERT INTO car " +
            "(id, brand, model, plate) values (null, ?, ?, ?)";

    @Inject
    private BookingCarStore bookingCarStore;

    public Car buyCar(Car car, Connection connection) {
        Car insertedCar = null;

        try (PreparedStatement st = connection.prepareStatement(INSERT_CAR, PreparedStatement.RETURN_GENERATED_KEYS)) {
            st.setString(1, car.getBrand());
            st.setString(2, car.getModel());
            st.setString(3, car.getPlate());
            st.execute();
            ResultSet rs = st.getGeneratedKeys();
            rs.first();
            insertedCar = new Car(rs.getInt(1), car.getBrand(), car.getModel(), car.getPlate());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return insertedCar;
    }
}
