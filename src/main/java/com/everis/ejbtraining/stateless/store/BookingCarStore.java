package com.everis.ejbtraining.stateless.store;

import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;

import javax.enterprise.context.ApplicationScoped;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;

/**
 * Created by hgranjal on 26/06/2018.
 */
@ApplicationScoped
public class BookingCarStore {

    private static final String FIND_ALL_CARS = "SELECT * FROM car";
    private static final String FIND_BOOKING_CAR = "SELECT * FROM bookingcar WHERE idCar = ?";
    private static final String FIND_ALL_BOOKING_CARS = "SELECT * FROM bookingcar";
    private static final String INSERT_BOOKING = "INSERT INTO bookingcar " +
            "(idcar, username) values (?,?)";
    private static final String REMOVE_BOOKING = "DELETE FROM bookingcar WHERE idCar = ?";


    public BookingCarStore() {

    }

    public List<Car> findCarAll(Connection connection) {
        List<Car> carList = new ArrayList<>();

        try(PreparedStatement st = connection.prepareStatement(FIND_ALL_CARS);
            ResultSet rs = st.executeQuery()) {

            while(rs.next()) {
                carList.add(new Car(rs.getInt("id"), rs.getString("brand"),
                        rs.getString("model"), rs.getString("plate")));
            }
        } catch (SQLException e) {
            System.out.println("Error getting cars from database: " + e.getMessage());
        }

        return carList;
    }

    public List<BookingCar> findBookingCarAll(Connection connection) {
        List<BookingCar> bookingCarList = new ArrayList<>();

        try(PreparedStatement st = connection.prepareStatement(FIND_ALL_BOOKING_CARS)) {
            ResultSet rs = st.executeQuery();
            while(rs.next()) {
                bookingCarList.add(new BookingCar(rs.getInt("idcar"), rs.getString("username")));
            }
        } catch (SQLException e) {
            System.out.println("Error getting bookings from database: " + e.getMessage());
        }

        return bookingCarList;
    }

    public void remove(BookingCar bookingCar, Connection connection) {

        try (PreparedStatement st = connection.prepareStatement(REMOVE_BOOKING)) {
            st.setInt(1, bookingCar.getIdCar());
            st.execute();
        } catch (SQLException e) {
            System.out.println("Error removing booking from database: " + e.getMessage());
        }
    }

    public boolean book(Car car, String user, Connection connection) {
        boolean inserted = false;

        try(PreparedStatement st = connection.prepareStatement(INSERT_BOOKING)) {
            st.setInt(1, car.getId());
            st.setString(2, user);
            inserted = st.execute();
        } catch (SQLException e) {
            System.out.println("Error inserting booking in database: " + e.getMessage());
        }

        return inserted;
    }

    public BookingCar findBookingCar(Car car, Connection connection) {
        BookingCar bookingCar = null;

        try(PreparedStatement st = connection.prepareStatement(FIND_BOOKING_CAR)) {
            st.setInt(1, car.getId());
            ResultSet rs = st.executeQuery();
            if (rs.first()) {
                bookingCar = new BookingCar(rs.getInt("idCar"), rs.getString("userName"));
            }
        } catch (SQLException e) {
            System.out.println("Error getting booking from database: " + e.getMessage());
        }

        return bookingCar;
    }
}
