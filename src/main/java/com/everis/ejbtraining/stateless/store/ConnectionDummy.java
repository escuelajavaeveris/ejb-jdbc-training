package com.everis.ejbtraining.stateless.store;


import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hgranjal on 26/06/2018.
 */
@Singleton
public class ConnectionDummy {

    private List<Car> carList = new ArrayList<>();
    private List<BookingCar> bookingCarList = new ArrayList<>();

    @PostConstruct
    public void initListas() {
        carList.add(new Car(1, "Renault", "4L", "1-XXX"));
        carList.add(new Car(2, "Ford", "Sierra", "2-XXX"));
        carList.add(new Car(3, "Audi", "A4", "3-XXX"));
        carList.add(new Car(4, "Mercedes", "Clase A", "4-XXX"));

        bookingCarList.add(new BookingCar(1, "Henrique"));
        bookingCarList.add(new BookingCar(4, "Carla"));
    }

    public List<Car> getCarList() {
        return carList;
    }

    public List<BookingCar> getBookingCarList() {
        return bookingCarList;
    }

    public void removeBookingCar(BookingCar bookingCar) {
        bookingCarList.remove(bookingCar);
    }

    public void addBookingCar(BookingCar bookingCar) {
        bookingCarList.add(bookingCar);
    }

    public void addCar(Car car) {
        carList.add(car);
    }
}
;