package com.everis.ejbtraining.stateless;

import com.everis.ejbtraining.stateless.model.BookingCar;
import com.everis.ejbtraining.stateless.model.Car;
import com.everis.ejbtraining.stateless.service.BookingCarService;
import com.everis.ejbtraining.stateless.service.ProviderCarService;
import junit.framework.Assert;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;

/**
 * Created by hgranjal on 26/06/2018.
 */
public class Application {

    private static EJBContainer ejbContainer;

    public static void main(String[] args) {
        try {
            ejbContainer = EJBContainer.createEJBContainer();
            BookingCarService bookingCarService = lookupBookingCarService();
            ProviderCarService providerCarService = lookupProviderCarService();
            System.out.println(bookingCarService.findCarAll("Marina"));
            Car patriciCar = new Car(1, "bbbb", "dddd", "eeee");
            BookingCar bookingCar = new BookingCar(60, "Patrici");
            providerCarService.buyCar(patriciCar);
            bookingCarService.book("Henrique", patriciCar, "Carla");
            bookingCarService.remove("Henrique", bookingCar);
        } catch (NamingException e) {
            e.printStackTrace();
        } finally {
            ejbContainer.close();
        }
    }

    private static BookingCarService lookupBookingCarService() throws NamingException {
        Object bookingCarService = ejbContainer.getContext().lookup("java:global/ejb-training/BookingCarServiceImpl");
        Assert.assertTrue(bookingCarService instanceof BookingCarService);
        return (BookingCarService) bookingCarService;
    }

    private static ProviderCarService lookupProviderCarService() throws NamingException {
        Object providerCarService = ejbContainer.getContext().lookup("java:global/ejb-training/ProviderCarServiceImpl");
        Assert.assertTrue(providerCarService instanceof ProviderCarService);
        return (ProviderCarService) providerCarService;
    }
}
